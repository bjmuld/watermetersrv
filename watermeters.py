#!/usr/bin/env python3

import argparse
import datetime
import os
import pathlib
import time

import smbus
from influxdb import InfluxDBClient

args = None

LOG = pathlib.Path('/var/log/watermeter-readings.csv.log')
MAX_LOG_SIZE = 100  # Megabytes


def clargs():
    global args
    ap = argparse.ArgumentParser()
    ap.add_argument('--debug', action='store_true')
    args, unargs = ap.parse_known_args()


clargs()


def get_raw_readings():
    bus = smbus.SMBus(1)
    address = 0x24
    data = [0x00] * 5
    readings = [0] * 3

    for i in range(len(data)):

        byte = None

        for attempt in range(10):

            try:
                byte = bus.read_byte_data(address, i)
                break

            except KeyboardInterrupt as e:
                raise e

            except IOError as e:
                # print(e)
                time.sleep(0.01)
                continue

        data[i] = byte

    readings[0] = data[0] + ((data[1] & 0x0F) << 8)
    readings[1] = ((data[1] & 0xF0) >> 4) + (data[2] << 4)
    readings[2] = data[3] + ((data[4] & 0x0F) << 8)

    return readings


def get_recent_vals(file):
    try:
        with open(file, 'rb') as f:
            f.seek(-2, os.SEEK_END)
            while f.read(1) != b'\n':
                f.seek(-2, os.SEEK_CUR)
            last_line = f.readline().decode()

        rvs = [int(i) for i in last_line.split(',')[1:]]

        return rvs

    except Exception:

        return None


def raw2cum(cur_raw, rec_raw, rec_cum):

    cums = [0]*3

    if args.debug:
        print("incoming raw vals: {}".format(cur_raw))
        print('latest raw values: {}'.format(rec_raw))
        print('latest cumuluative values: {}'.format(rec_cum))

    # check for rollover
    if rec_raw is not None:
        for i, (cr, rr, rc) in enumerate(zip(cur_raw, rec_raw, rec_cum)):

            if args.debug:
                print('cur-raw: {}, prev-raw: {}, prev-cum: {}'.format(cr, rr, rc))

            if cr < rr:
                cr += 4096
                cums[i] = rc + (cr - rr)

            else:
                cums[i] = rc + (cr - rr)

        if args.debug:
            print('cumulative vals: {},'.format(cums))

    return cums


def update_log(file, value_vect):
    if not isinstance(file, pathlib.Path):
        file = pathlib.Path(file)

    if not file.is_file():
        file.touch()

    if os.stat(file).st_size > MAX_LOG_SIZE * 2 ** 20:  # (megabytes)
        with open(file, 'w') as f:
            f.write(
                ','.join([
                    str(datetime.datetime.now().replace(microsecond=0).isoformat()),
                    *[str(v) for v in value_vect],
                ]) + '\n'
            )

    else:
        with open(file, 'a') as f:
            f.write(
                ','.join([
                    str(datetime.datetime.now().replace(microsecond=0).isoformat()),
                    *[str(v) for v in value_vect],
                ]) + '\n'
            )


def last_recorded_grafana(measurement):
    database = InfluxDBClient(host='hub.689berne.net', port=8086, username='meter', password='yH6rC82QjdZ8HNiPh4gwAdh',
                              database='watermeters')

    resp = database.query('SELECT last("value") FROM "{}" GROUP BY "apartment";'.format(measurement))

    rrvs = (
        list(resp.get_points(tags={'apartment': 'A'}))[-1]['last'],
        list(resp.get_points(tags={'apartment': 'B'}))[-1]['last'],
        list(resp.get_points(tags={'apartment': 'C'}))[-1]['last'],
    )

    if args.debug:
        print('got recent \"{}\" vals from grafana: {}'.format(measurement, rrvs))

    return rrvs


def update_grafana(measurement, values):
    assert isinstance(measurement, str)

    json_data = [
        {
            "measurement": measurement,
            "tags": {"apartment": "A"},
            "fields": {
                "value": values[0],
            },
        },
        {
            "measurement": measurement,
            "tags": {"apartment": "B"},
            "fields": {
                "value": values[1],
            },
        },
        {
            "measurement": measurement,
            "tags": {"apartment": "C"},
            "fields": {
                "value": values[2],
            },
        }
    ]

    database = InfluxDBClient(host='hub.689berne.net', port=8086, username='meter', password='yH6rC82QjdZ8HNiPh4gwAdh',
                              database='watermeters')
    database.write_points(json_data)


def main():

    # get recent raw values from local log:
    rvs = get_recent_vals(LOG)
    if rvs is None:
        rvs = [0]*6
    rrv = rvs[:3]
    rcv = rvs[3:]

    # recent raw values from grafana
    rrv_g = last_recorded_grafana('raw_readings')
    rcv_g = last_recorded_grafana('water')

    # get current raw values:
    crv = get_raw_readings()

    # check for activity
    if rrv == crv and rrv == rrv_g:
        print("no change in reading")
        return

    # correct for any rollover that might have occurred
    ccv = raw2cum(crv, rrv, rcv)

    try:
        update_grafana('raw_readings', crv)
    except:
        pass

    try:
        update_grafana('water', ccv)
    except:
        pass

    update_log(LOG, crv + ccv)

    print('raw readings: {}'.format(crv))
    print('cumulative readings: {}'.format(ccv))


if __name__ == '__main__':
    main()
